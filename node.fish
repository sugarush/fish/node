if ! test -f ~/.npmrc
  echo '
    prefix=${HOME}/.npm
    global-dir=${HOME}/.npm
    global-bin-dir=${HOME}/.npm/bin
  ' > ~/.npmrc
end

if ! test -d ~/.npm
  mkdir ~/.npm
end

if ! test -d ~/.npm/bin
  mkdir ~/.npm/bin
end

if ! contains ~/.npm/bin $PATH
  set -a PATH ~/.npm/bin
end
